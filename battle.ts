import Bot from './bots/Bot.ts'
import Cycler from './bots/example/Cycler.ts'
import { battle } from './src/RockPaperScissors.ts'

battle(Bot(), Cycler())
