# Rock Paper Scissors

Framework for running Rock-Paper-Scissors bot tournaments

## Instructions

1. Fork https://gitlab.com/bquinn_precoa/rock-paper-scissors
2. [Install Deno](https://deno.land/manual@v1.29.4/getting_started/installation)
3. Customize the `Bot.ts` in the `bots` directory
4. Run `deno run battle.ts` to see how it fares against an example bot

## Submitting a bot

1. Push your changes to a public git repo or any public spot on the web.
2. Send me the link to the raw file. Example: `https://gitlab.com/bquinn_precoa/rock-paper-scissors/-/raw/main/bots/Bot.ts`

## Bots must not:

- Generate errors
- Cause significant delays
- Output to the console
- Use a random number generator
- Simulate random output by iterating through a list of responses

## Participants may not:

- Research Rock-paper-scissors strategies on the internet
- Look at other players' source code

## Tournament Format

- Participants will have one hour to write the best bot possible, or experiment with multiple bot strategies
- At the 45-minute mark, we will check in and allow more time if desired

### Scramble

- Participants can submit as many bots as they want for the unofficial scramble
- There will be no prize
- Experimental/crazy strategies welcome!
- Games will go to 10 points

### Grand Prix

- Each participant will select one of their bots to compete in the Grand Prix Tournament. This must be chosen before the scramble is run!
- The number of throws per tournament game will not be disclosed ahead of time but it will be much greater than in the Scramble
- The winner will receive a fabulous prize!
