import { BotFactory } from '../../src/Types.d.ts'

const bot: BotFactory = () => {
	const moves = ['rock', 'paper', 'scissors']
	let moveIndex = 0

	const Report = (myMove, theirMove, result) => {
	}

	const Shoot = () => {
		const move = moves[moveIndex]
		moveIndex = (moveIndex + 1) % 3
		return move
	}

	return {
		Name: 'Cycler',
		Report,
		Shoot,
	}
}

export default bot
