export default () => {
	const Report = (myMove, theirMove, result) => {
	}
	const Shoot = () => {
		const r = Math.random()

		if (r < 1 / 3) {
			return 'rock'
		}

		if (r < 2 / 3) {
			return 'paper'
		}

		return 'scissors'
	}

	return {
		Name: 'Random',
		Report,
		Shoot,
	}
}
