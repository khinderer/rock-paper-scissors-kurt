import { BotFactory } from '../src/Types.d.ts'

const bot: BotFactory = () => {
	let theirLastMove = ''
	let lastResult = 0
	let myLastMove = ''

	// Called after each round with the results
	// result is 1 for win, -1 for loss, 0 for tie
	const Report = (myMove: string, theirMove: string, result: int) => {
		lastResult = result
		theirLastMove = theirMove
		myLastMove = myMove
	}

	const Shoot = () => {
		let nextMove = 'paper'
		if(lastResult == 0 && myLastMove != '')
		{
			nextMove = myLastMove
		}
		else if(lastResult == 1)
		{
			if(myLastMove == 'rock')
			{
				nextMove = 'scissors'
			}
			else if(myLastMove == 'paper')
			{
				nextMove = 'rock'
			}
		}
		else if(lastResult == -1)
		{
			nextMove = theirLastMove
		}


		return nextMove
	}

	return {
		Name: 'Name Pending',
		Shoot,
		Report,
	}
}

export default bot
